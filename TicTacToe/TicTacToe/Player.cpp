#include "stdafx.h"
#include "GameObject.h"
#include <iostream>
#include <string>

using namespace std;

string playerName;
int playerType; // 0 = human | 1 = computer
char playerIcon;

Player::Player()
{
	
}

Player::Player(int playerNum)
{
	playerType = 0;
	if (playerNum == 1)
		playerIcon = 'X';
	else
		playerIcon = 'O';
}

void Player::setPlayerName(string name)
{
	playerName = name;
}

string Player::getPlayerName()
{
	return playerName;
}

void Player::setPlayerType(int type)
{
	playerType = type;
}

int Player::getPlayerType()
{
	return playerType;
}

char Player::getPlayerIcon()
{
	return playerIcon;
}