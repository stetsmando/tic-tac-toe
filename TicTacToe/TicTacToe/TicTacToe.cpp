#include "stdafx.h"
#include <Windows.h>
#include "GameObject.h"

using namespace std;

int main()
{
	GameObject myGame = GameObject();
	myGame.playIntro();
	myGame.setUpGame();
	myGame.startGame();

	system("PAUSE");
	return 0;
}