#pragma once
#include "stdafx.h"
#include "GameObject.h"
#include <iostream>
#include <string>

using namespace std;

class GameObject
{
public:
	GameObject();
	~GameObject();
	void playIntro();
	void setUpGame();
	void startGame();
};

class Intro
{
public:
	Intro();
	void playIntro();
	void playSetUp();
	void playOutro();
};

class GameBoard
{
public:
	GameBoard();
	void paintGameBoard();
	void paintGameBoard(string playerName);
	char getGameSquare(int index);
	void setGameSquare(char gameSqr, char playerIcon);
	int checkMove(char playerMove);
	int checkWin();
	void reset();
};

class Player
{
public:
	Player();
	Player(int);
	void setPlayerName( string );
	string getPlayerName();
	void setPlayerType( int );
	int getPlayerType();
	char getPlayerIcon();

private:
	string playerName;
	int playerType;
	char playerIcon;
};