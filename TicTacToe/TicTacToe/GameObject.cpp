#include "stdafx.h"
#include <iostream>
#include <string>
#include <Windows.h>
#include <time.h>
#include "GameObject.h"

using namespace std;

enum GameState { NO_STATE, IN_GAME, GAME_OVER };
enum GameType { NO_TYPE, ONE_P, TWO_P };

GameState gameState = NO_STATE;
GameType gameType = NO_TYPE;

Intro gameIntro;
GameBoard gameBoard;
Player playerOne;
Player playerTwo;

Player players[2] = {playerOne, playerTwo};
int currentPlayer; //'0' - Player 1 and '1' - Player 2

GameObject::GameObject()
{
	gameIntro = Intro();
	gameBoard = GameBoard();

	playerOne = Player(1);
	playerTwo = Player(2);
}

GameObject::~GameObject()
{

}

void GameObject::playIntro()
{
	gameIntro.playIntro();
}

void GameObject::setUpGame()
{
	string action;
	do
	{
		gameIntro.playSetUp();
		cin.sync();
		cin >> action;

	} while (action[0] != '1' && action[0] != '2');
	string playerName;

	cout << "Player 1, please enter your name: ";
	cin.sync();
	cin >> playerName;
	cout << "Thank you, " << playerName << "." << endl;
	playerOne.setPlayerName(playerName);

	if (action[0] == '1')
	{
		//One player game, computer needed
		Sleep(1000);
		gameType = ONE_P;
		playerTwo.setPlayerType(1);
		playerTwo.setPlayerName("AWESOM-O 4000");
		cout << "\n" << playerOne.getPlayerName() << ", today you'll be playing against ";
		cout << "your new best frined, AWESOM-O 4000." << endl;
		cout << "He's been sent all the way from Japan to serve as your personal robot..." << endl;
		cout << "Who also happens to kick ass at tic-tac-toe!" << endl;
		Sleep(2000);
	}
	else
	{
		//Two player game, both players are human
		gameType = TWO_P;
		cout << "\nPlayer 2, please enter your name: ";
		cin.sync();
		cin >> playerName;
		cout << "Thank you, " << playerName << "." << endl;
		playerTwo.setPlayerName(playerName);
	}
	
	//Randomize order of players
	srand(time(0));
	currentPlayer = rand() % 2;
	players[0] = playerOne;
	players[1] = playerTwo;
	cout << "\n\nPress ENTER to continue...";
	cin.sync();
	cin.get();
}

void GameObject::startGame()
{
	int gameWon = 0;
	if (gameType == ONE_P)
	{
		//One player game
	}
	else
	{
		//Two player game
		do
		{
			gameBoard.paintGameBoard(players[currentPlayer].getPlayerName());
			int isValid;
			string playerMove;
			cin.sync();
			cin >> playerMove;

			isValid = gameBoard.checkMove(playerMove[0]);	//Checks if player's move is valid
															//Returns 0 if valid, -1 if not
			if (isValid == 0)
			{
				gameBoard.setGameSquare(playerMove[0], players[currentPlayer].getPlayerIcon());
			}
			else
			{
				continue;
			}
			//Will return	0 - game on going 
			//				1 - game over - WIN
			//				2 - game over - DRAW
			gameWon = gameBoard.checkWin();

			if (gameWon == 0)
			{
				//Swap players
				if (currentPlayer == 1)
					currentPlayer -= 1;
				else
					currentPlayer += 1;
			}
		} while (gameWon != 1 && gameWon != 2);
		
				
	}// End of Two Player Game
	
	gameBoard.paintGameBoard();
	
	if (gameWon == 1)
	{
		//Game over and current player is winner
		cout << "Game over! " << players[currentPlayer].getPlayerName() << " is the winner!" << endl;
	}
	else if (gameWon == 2)
	{
		//Game over and there is a draw
		cout << "Looks like it's a draw!" << endl;
	}

	string replay;
	cout << "Want to play again? (y/n): ";
	cin.sync();
	cin >> replay;
	if (replay[0] == 'y' || replay[0] == 'Y')
	{
		gameBoard.reset();
		GameObject::startGame();
	}

} // end of startGame()