#include "stdafx.h"
#include "GameObject.h"
#include <iostream>
#include <Windows.h>

using namespace std;

Intro::Intro()
{

}

void Intro::playIntro()
{
	string programVersion = "Dev: Stetson Pierce v0.1.1 (Objects)";
	string introText[10] = {
		programVersion,
		"\n\n\n\n     |     |     ",
		"\n  T  |  I  |  C",
		"\n_____|_____|_____",
		"\n     |     |     ",
		"\n  T  |  A  |  C",
		"\n_____|_____|_____",
		"\n     |     |     ",
		"\n  T  |  O  |  E",
		"\n     |     |     "
	};
	int numberOfElements = sizeof(introText) / sizeof(introText[0]);
	string displayText;

	for (int i = 0; i < numberOfElements; i++)
	{
		displayText += introText[i];

		system("cls");
		cout << displayText;
		Sleep(500);
	}

	cout << "\n\nPress ENTER to begin...";
	cin.get();
}

void Intro::playSetUp()
{
	system("cls");
	cout << "Tic Tac Toe - Set Up" << endl << endl;
	cout << "Please select the type of game you wish to play." << endl << endl;
	cout << "\t1 - Single Player" << endl << "\t2 - Two Player" << endl << endl;

	cout << "Enter game type: ";
}

void Intro::playOutro()
{

}