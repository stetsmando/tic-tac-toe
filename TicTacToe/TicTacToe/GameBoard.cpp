#include "stdafx.h"
#include <iostream>
#include <string>
#include <Windows.h>
#include "GameObject.h"

using namespace std;

char gameSquareInitial[10] = { '0', '7', '8', '9', '4', '5', '6', '1', '2', '3' };
char gameSquare[10] = { '0', '7', '8', '9', '4', '5', '6', '1', '2', '3' };

GameBoard::GameBoard()
{

}

void GameBoard::paintGameBoard()
{
	system("cls");
	cout << "Tic Tac Toe" << endl << endl;
	cout << "     |     |     " << endl;
	cout << "  " << gameSquare[1] << "  |  " << gameSquare[2] << "  |  " << gameSquare[3] << endl;
	cout << "_____|_____|_____" << endl;
	cout << "     |     |     " << endl;
	cout << "  " << gameSquare[4] << "  |  " << gameSquare[5] << "  |  " << gameSquare[6] << endl;
	cout << "_____|_____|_____" << endl;
	cout << "     |     |     " << endl;
	cout << "  " << gameSquare[7] << "  |  " << gameSquare[8] << "  |  " << gameSquare[9] << endl;
	cout << "     |     |     " << endl << endl;
}

void GameBoard::paintGameBoard(string playerName)
{
	system("cls");
	cout << "Tic Tac Toe" << endl << endl;
	cout << "     |     |     " << endl;
	cout << "  " << gameSquare[1] << "  |  " << gameSquare[2] << "  |  " << gameSquare[3] << endl;
	cout << "_____|_____|_____" << endl;
	cout << "     |     |     " << endl;
	cout << "  " << gameSquare[4] << "  |  " << gameSquare[5] << "  |  " << gameSquare[6] << endl;
	cout << "_____|_____|_____" << endl;
	cout << "     |     |     " << endl;
	cout << "  " << gameSquare[7] << "  |  " << gameSquare[8] << "  |  " << gameSquare[9] << endl;
	cout << "     |     |     " << endl << endl;
	cout << playerName << " enter your move: ";
}

char GameBoard::getGameSquare(int index)
{
	switch (index)
	{
	case 1:
		return gameSquare[1];
		break;

	case 2:
		return gameSquare[2];
		break;

	case 3:
		return gameSquare[3];
		break;

	case 4:
		return gameSquare[4];
		break;

	case 5:
		return gameSquare[5];
		break;

	case 6:
		return gameSquare[6];
		break;

	case 7:
		return gameSquare[7];
		break;

	case 8:
		return gameSquare[8];
		break;

	case 9:
		return gameSquare[9];
		break;
	}
}

void GameBoard::setGameSquare(char gameSqr, char playerIcon)
{
	switch (gameSqr)
	{
	case '1':
		gameSquare[7] = playerIcon;
		break;
	case '2':
		gameSquare[8] = playerIcon;
		break;
	case '3':
		gameSquare[9] = playerIcon;
		break;
	case '4':
		gameSquare[4] = playerIcon;
		break;
	case '5':
		gameSquare[5] = playerIcon;
		break;
	case '6':
		gameSquare[6] = playerIcon;
		break;
	case '7':
		gameSquare[1] = playerIcon;
		break;
	case '8':
		gameSquare[2] = playerIcon;
		break;
	case '9':
		gameSquare[3] = playerIcon;
		break;
	default:
		break;
	}
}

int GameBoard::checkMove(char playerMove)
{
	int validMove = 0; //Valid if it remains 0

	switch (playerMove)
	{
	case '1':
		if (gameSquare[7] != '1')
			validMove = -1;
		break;
	case '2':
		if (gameSquare[8] != '2')
			validMove = -1;
		break;
	case '3':
		if (gameSquare[9] != '3')
			validMove = -1;
		break;
	case '4':
		if (gameSquare[4] != '4')
			validMove = -1;
		break;
	case '5':
		if (gameSquare[5] != '5')
			validMove = -1;
		break;
	case '6':
		if (gameSquare[6] != '6')
			validMove = -1;
		break;
	case '7':
		if (gameSquare[1] != '7')
			validMove = -1;
		break;
	case '8':
		if (gameSquare[2] != '8')
			validMove = -1;
		break;
	case '9':
		if (gameSquare[3] != '9')
			validMove = -1;
		break;
	default:
		validMove = -1;
		break;
	}

	if (validMove == -1)
	{
		cout << "Sorry that's not valid!" << endl;
		Sleep(1500);
	}

	return validMove;
}

int GameBoard::checkWin()
{
	int gameWon = 0;	//Remains 0 if game ongoing
						//Return 1 - Game Over - Winner
						//Return 2 - Game Over - Draw
	if (gameSquare[1] == gameSquare[4] && gameSquare[4] == gameSquare[7])
		gameWon = 1;
	else if (gameSquare[2] == gameSquare[5] && gameSquare[5] == gameSquare[8])
		gameWon = 1;
	else if (gameSquare[3] == gameSquare[6] && gameSquare[6] == gameSquare[9])
		gameWon = 1;
	else if (gameSquare[1] == gameSquare[2] && gameSquare[2] == gameSquare[3])
		gameWon = 1;
	else if (gameSquare[4] == gameSquare[5] && gameSquare[5] == gameSquare[6])
		gameWon = 1;
	else if (gameSquare[7] == gameSquare[8] && gameSquare[8] == gameSquare[9])
		gameWon = 1;
	else if (gameSquare[1] == gameSquare[5] && gameSquare[5] == gameSquare[9])
		gameWon = 1;
	else if (gameSquare[3] == gameSquare[5] && gameSquare[5] == gameSquare[7])
		gameWon = 1;
	else if (gameSquare[1] != '7' && gameSquare[2] != '8' && gameSquare[3] != '9' && 
		gameSquare[4] != '4' && gameSquare[5] != '5' && gameSquare[6] != '6' && 
		gameSquare[7] != '1' && gameSquare[8] != '2' && gameSquare[9] != '3')
			gameWon = 2;

	return gameWon;
}

void GameBoard::reset()
{
	for (int i = 1; i <= 10; i++)
	{
		gameSquare[i] = gameSquareInitial[i];
	}
}
